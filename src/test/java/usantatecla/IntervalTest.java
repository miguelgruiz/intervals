package usantatecla;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class IntervalTest {
  
  private Point left;
  private Point right;
  private IntervalBuilder intervalBuilder;

  @BeforeEach
  public void before(){
    this.left = new Point(-2.2);
    this.right = new Point(4.4);
    this.intervalBuilder = new IntervalBuilder();
  }

  @Test
  public void givenIntervalOpenOpenwhenIncludeWithIncludedValueThenTrue() {
    Interval interval = this.intervalBuilder.open(left.getEquals()).open(right.getEquals()).build();
    assertFalse(interval.include(left.getLess()));
    assertFalse(interval.include(left.getEquals()));
    assertTrue(interval.include(left.getGreater()));
    assertTrue(interval.include(right.getLess()));
    assertFalse(interval.include(right.getEquals()));
    assertFalse(interval.include(right.getGreater()));
  }

  @Test
  public void givenIntervalClosedOpenwhenIncludeWithIncludedValueThenTrue() {
    Interval interval = this.intervalBuilder.closed(left.getEquals()).open(right.getEquals()).build();
    assertFalse(interval.include(left.getLess()));
    assertTrue(interval.include(left.getEquals()));
    assertTrue(interval.include(left.getGreater()));

    assertTrue(interval.include(right.getLess()));
    assertFalse(interval.include(right.getEquals()));
    assertFalse(interval.include(right.getGreater()));
  }

  @Test
  public void givenIntervalOpenClosedwhenIncludeWithIncludedValueThenTrue() {
    Interval interval = this.intervalBuilder.open(left.getEquals()).closed(right.getEquals()).build();
    assertFalse(interval.include(left.getLess()));
    assertFalse(interval.include(left.getEquals()));
    assertTrue(interval.include(left.getGreater()));

    assertTrue(interval.include(right.getLess()));
    assertTrue(interval.include(right.getEquals()));
    assertFalse(interval.include(right.getGreater()));
  }

  @Test
  public void givenIntervalClosedClosedwhenIncludeWithIncludedValueThenTrue() {
    Interval interval = this.intervalBuilder.closed(left.getEquals()).closed(right.getEquals()).build();
    assertFalse(interval.include(left.getLess()));
    assertTrue(interval.include(left.getEquals()));
    assertTrue(interval.include(left.getGreater()));

    assertTrue(interval.include(right.getLess()));
    assertTrue(interval.include(right.getEquals()));
    assertFalse(interval.include(right.getGreater()));
  }

  @Test
  public void givenIntervalOpenOpenWhenIsIntersectedWithIntervalOpenOpenIncludedValueThenTrue() {
    Interval interval = this.intervalBuilder.open(left.getEquals()).open(right.getEquals()).build();
    assertFalse(interval.isIntersected(new IntervalBuilder().open(left.getLess()).open(left.getEquals()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().open(right.getEquals()).open(right.getGreater()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().open(left.getLess()).open(left.getLess()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().open(right.getGreater()).open(right.getGreater()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().open(left.getEquals()).open(right.getEquals()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().open(left.getLess()).open(right.getGreater()).build()));

  }
  @Test
  public void givenIntervalOpenOpenWhenIsIntersectedWithIntervalOpenClosedIncludedValueThenTrue() {
    Interval interval = this.intervalBuilder.open(left.getEquals()).open(right.getEquals()).build();
    assertTrue(interval.isIntersected(new IntervalBuilder().open(left.getLess()).closed(left.getEquals()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().open(right.getEquals()).closed(right.getGreater()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().open(left.getLess()).closed(left.getLess()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().open(right.getGreater()).closed(right.getGreater()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().open(left.getEquals()).closed(right.getEquals()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().open(left.getLess()).closed(right.getGreater()).build()));
  }
  @Test
  public void givenIntervalOpenOpenWhenIsIntersectedWithIntervalClosedOpenIncludedValueThenTrue() {
    Interval interval = this.intervalBuilder.open(left.getEquals()).open(right.getEquals()).build();
    assertFalse(interval.isIntersected(new IntervalBuilder().closed(left.getLess()).open(left.getEquals()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().closed(right.getEquals()).open(right.getGreater()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().closed(left.getLess()).open(left.getLess()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().closed(right.getGreater()).open(right.getGreater()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().closed(left.getEquals()).open(right.getEquals()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().closed(left.getLess()).open(right.getGreater()).build()));
  }
  @Test
  public void givenIntervalOpenOpenWhenIsIntersectedWithIntervalClosedClosedIncludedValueThenTrue() {
    Interval interval = this.intervalBuilder.open(left.getEquals()).open(right.getEquals()).build();
    assertTrue(interval.isIntersected(new IntervalBuilder().closed(left.getLess()).closed(left.getEquals()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().closed(right.getEquals()).closed(right.getGreater()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().closed(left.getLess()).closed(left.getLess()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().closed(right.getGreater()).closed(right.getGreater()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().closed(left.getEquals()).closed(right.getEquals()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().closed(left.getLess()).closed(right.getGreater()).build()));
  }
  @Test
  public void givenIntervalOpenClosedWhenIsIntersectedWithIntervalOpenOpenIncludedValueThenTrue() {
    Interval interval = this.intervalBuilder.open(left.getEquals()).closed(right.getEquals()).build();
    assertFalse(interval.isIntersected(new IntervalBuilder().open(left.getLess()).open(left.getEquals()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().open(right.getEquals()).open(right.getGreater()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().open(left.getLess()).open(left.getLess()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().open(right.getGreater()).open(right.getGreater()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().open(left.getEquals()).open(right.getEquals()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().open(left.getLess()).open(right.getGreater()).build()));
  }
  @Test
  public void givenIntervalOpenClosedWhenIsIntersectedWithIntervalOpenClosedIncludedValueThenTrue() {
    Interval interval = this.intervalBuilder.open(left.getEquals()).closed(right.getEquals()).build();
    assertTrue(interval.isIntersected(new IntervalBuilder().open(left.getLess()).closed(left.getEquals()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().open(right.getEquals()).closed(right.getGreater()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().open(left.getLess()).closed(left.getLess()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().open(right.getGreater()).closed(right.getGreater()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().open(left.getEquals()).closed(right.getEquals()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().open(left.getLess()).closed(right.getGreater()).build()));
  }
  @Test
  public void givenIntervalOpenClosedWhenIsIntersectedWithIntervalClosedOpenIncludedValueThenTrue() {
    Interval interval = this.intervalBuilder.open(left.getEquals()).closed(right.getEquals()).build();
    assertFalse(interval.isIntersected(new IntervalBuilder().closed(left.getLess()).open(left.getEquals()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().closed(right.getEquals()).open(right.getGreater()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().closed(left.getLess()).open(left.getLess()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().closed(right.getGreater()).open(right.getGreater()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().closed(left.getEquals()).open(right.getEquals()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().closed(left.getLess()).open(right.getGreater()).build()));
  }
  @Test
  public void givenIntervalOpenClosedWhenIsIntersectedWithIntervalClosedClosedIncludedValueThenTrue() {
    Interval interval = this.intervalBuilder.open(left.getEquals()).closed(right.getEquals()).build();
    assertTrue(interval.isIntersected(new IntervalBuilder().closed(left.getLess()).closed(left.getEquals()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().closed(right.getEquals()).closed(right.getGreater()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().closed(left.getLess()).closed(left.getLess()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().closed(right.getGreater()).closed(right.getGreater()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().closed(left.getEquals()).closed(right.getEquals()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().closed(left.getLess()).closed(right.getGreater()).build()));
  }
  @Test
  public void givenIntervalClosedOpenWhenIsIntersectedWithIntervalOpenOpenIncludedValueThenTrue() {
    Interval interval = this.intervalBuilder.closed(left.getEquals()).open(right.getEquals()).build();
    assertTrue(interval.isIntersected(new IntervalBuilder().open(left.getLess()).open(left.getEquals()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().open(right.getEquals()).open(right.getGreater()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().open(left.getLess()).open(left.getLess()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().open(right.getGreater()).open(right.getGreater()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().open(left.getEquals()).open(right.getEquals()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().open(left.getLess()).open(right.getGreater()).build()));
  }
  @Test
  public void givenIntervalClosedOpenWhenIsIntersectedWithIntervalOpenClosedIncludedValueThenTrue() {
    Interval interval = this.intervalBuilder.closed(left.getEquals()).open(right.getEquals()).build();
    assertTrue(interval.isIntersected(new IntervalBuilder().open(left.getLess()).closed(left.getEquals()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().open(right.getEquals()).closed(right.getGreater()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().open(left.getLess()).closed(left.getLess()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().open(right.getGreater()).closed(right.getGreater()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().open(left.getEquals()).closed(right.getEquals()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().open(left.getLess()).closed(right.getGreater()).build()));
  }
  @Test
  public void givenIntervalClosedOpenWhenIsIntersectedWithIntervalClosedOpenIncludedValueThenTrue() {
    Interval interval = this.intervalBuilder.closed(left.getEquals()).open(right.getEquals()).build();
    assertTrue(interval.isIntersected(new IntervalBuilder().closed(left.getLess()).open(left.getEquals()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().closed(right.getEquals()).open(right.getGreater()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().closed(left.getLess()).open(left.getLess()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().closed(right.getGreater()).open(right.getGreater()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().closed(left.getEquals()).open(right.getEquals()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().closed(left.getLess()).open(right.getGreater()).build()));
  }
  @Test
  public void givenIntervalClosedOpenWhenIsIntersectedWithIntervalClosedClosedIncludedValueThenTrue() {
    Interval interval = this.intervalBuilder.closed(left.getEquals()).open(right.getEquals()).build();
    assertTrue(interval.isIntersected(new IntervalBuilder().closed(left.getLess()).closed(left.getEquals()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().closed(right.getEquals()).closed(right.getGreater()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().closed(left.getLess()).closed(left.getLess()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().closed(right.getGreater()).closed(right.getGreater()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().closed(left.getEquals()).closed(right.getEquals()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().closed(left.getLess()).closed(right.getGreater()).build()));
  }
  @Test
  public void givenIntervalClosedClosedWhenIsIntersectedWithIntervalOpenOpenIncludedValueThenTrue() {
    Interval interval = this.intervalBuilder.closed(left.getEquals()).closed(right.getEquals()).build();
    assertTrue(interval.isIntersected(new IntervalBuilder().open(left.getLess()).open(left.getEquals()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().open(right.getEquals()).open(right.getGreater()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().open(left.getLess()).open(left.getLess()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().open(right.getGreater()).open(right.getGreater()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().open(left.getEquals()).open(right.getEquals()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().open(left.getLess()).open(right.getGreater()).build()));
  }
  @Test
  public void givenIntervalClosedClosedWhenIsIntersectedWithIntervalOpenClosedIncludedValueThenTrue() {
    Interval interval = this.intervalBuilder.closed(left.getEquals()).closed(right.getEquals()).build();
    assertTrue(interval.isIntersected(new IntervalBuilder().open(left.getLess()).closed(left.getEquals()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().open(right.getEquals()).closed(right.getGreater()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().open(left.getLess()).closed(left.getLess()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().open(right.getGreater()).closed(right.getGreater()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().open(left.getEquals()).closed(right.getEquals()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().open(left.getLess()).closed(right.getGreater()).build()));
  }
  @Test
  public void givenIntervalClosedClosedWhenIsIntersectedWithIntervalClosedOpenIncludedValueThenTrue() {
    Interval interval = this.intervalBuilder.closed(left.getEquals()).closed(right.getEquals()).build();
    assertTrue(interval.isIntersected(new IntervalBuilder().closed(left.getLess()).open(left.getEquals()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().closed(right.getEquals()).open(right.getGreater()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().closed(left.getLess()).open(left.getLess()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().closed(right.getGreater()).open(right.getGreater()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().closed(left.getEquals()).open(right.getEquals()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().closed(left.getLess()).open(right.getGreater()).build()));
  }
  @Test
  public void givenIntervalClosedClosedWhenIsIntersectedWithIntervalClosedClosedIncludedValueThenTrue() {
    Interval interval = this.intervalBuilder.closed(left.getEquals()).closed(right.getEquals()).build();
    assertTrue(interval.isIntersected(new IntervalBuilder().closed(left.getLess()).closed(left.getEquals()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().closed(right.getEquals()).closed(right.getGreater()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().closed(left.getLess()).closed(left.getLess()).build()));
    assertFalse(interval.isIntersected(new IntervalBuilder().closed(right.getGreater()).closed(right.getGreater()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().closed(left.getEquals()).closed(right.getEquals()).build()));
    assertTrue(interval.isIntersected(new IntervalBuilder().closed(left.getLess()).closed(right.getGreater()).build()));
  }

}